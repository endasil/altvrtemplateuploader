﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace AltVRTemplateUploaderExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // https://postman-echo.com/put
            var response = ExecutePutCommand("C:/Dev/Unity/AltspaceVr test word/template.zip", "https://account.altvr.com/api/space_templates/<INSERT_YOUR_TEMPLATE_ID_HERE>.json");
        }

        protected static string ExecutePutCommand(string nameOfTemplateZip, string altVRTemplateUrl)
        {
            byte[] fileBytes = File.ReadAllBytes(nameOfTemplateZip);
            
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(altVRTemplateUrl);
            request.PreAuthenticate = true;

            string boundary = System.Guid.NewGuid().ToString();
            
            request.ContentType = string.Format("multipart/form-data;boundary={0}", boundary);
            request.Method = "PUT";
            request.Headers["Cookie"] =
                "ASLBSA=<INSERT_INFO_FROM_COOKIE_HERE>; ASLBSACORS=<INSERT_INFO_FROM_COOKIE_HERE>; _Positron_session=<INSERT_INFO_FROM_COOKIE_HERE>";

            string header = string.Format("--{0}", boundary);
            string footer = header + "--";
            
            // Information about the template file to upload
            StringBuilder contents = new StringBuilder();
            contents.AppendLine(header);
            contents.AppendLine(string.Format("Content-Disposition:form-data; name=\"space_template[zip]\"; filename=\"{0}\"", nameOfTemplateZip));
            contents.AppendLine("Content-Type: application/x-zip-compressed");
            contents.AppendLine();
            
            // Set unity game engine version
            StringBuilder contents2 = new StringBuilder();
            contents2.AppendLine();
            contents2.AppendLine(header);
            contents2.AppendLine("Content-Disposition:form-data; name=\"space_template[game_engine_version]\"");
            contents2.AppendLine();
            contents2.AppendLine("20194");
            // Form Field 2
            // Footer
            contents2.AppendLine(footer);

            
            byte[] templateFileInfoBytes = Encoding.UTF8.GetBytes(contents.ToString());
            byte[] gameEngineInfoBytes = Encoding.UTF8.GetBytes(contents2.ToString());

            // now we have all of the bytes we are going to send, so we can calculate the size of the stream
            request.ContentLength = templateFileInfoBytes.Length + fileBytes.Length + gameEngineInfoBytes.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(templateFileInfoBytes, 0, templateFileInfoBytes.Length);
                requestStream.Write(fileBytes, 0, fileBytes.Length);
                requestStream.Write(gameEngineInfoBytes, 0, gameEngineInfoBytes.Length);
                requestStream.Flush();
                requestStream.Close();
                try
                {
                    using (WebResponse response = request.GetResponse())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            return  reader.ReadToEnd();
                        }
                    }
                }
                catch (WebException ex)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        var txt = reader.ReadToEnd();
                        Console.WriteLine(txt);
                    }
                }
            }

            return null; // shouldn't get here
        }
    }
}
